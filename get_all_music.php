<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
    // get favorite song list for the current user
    
    $stmt = $mysqli->prepare("select * from favorite_songs where user_id = ?;");
    
    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }
	$stmt->bind_param('s', $_SESSION['current_user']);
    $stmt->execute();
    $query_result = $stmt->get_result();

	$favorites = array();

    while($row = $query_result->fetch_assoc()){
        $temp = array("artist" => $row["artist"]);
		$song_name = $row['name'];
    	$favorites[$song_name] = $temp;
    }
    
    /////////

    $stmt = $mysqli->prepare("select * from songs;");
    
    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

    $stmt->execute();
    $query_result = $stmt->get_result();

    $output_json = array();

    while($row = $query_result->fetch_assoc()){
    	$song_name = $row['name'];
    	
    	if(array_key_exists($song_name, $favorites)){
    	    $temp = array("artist" => $row["artist"], "soundcloud_src" => $row["soundcloud_src"], "likes" => $row["likes"], "listens" => $row["listens"], "favorite" => true);
    		$output_json[$song_name] = $temp;
    	}
    	else{
    	    $temp = array("artist" => $row["artist"], "soundcloud_src" => $row["soundcloud_src"], "likes" => $row["likes"], "listens" => $row["listens"], "favorite" => false);
    		$output_json[$song_name] = $temp;
    	}
    	
    }
    
    echo json_encode($output_json);
    $stmt->close();
    exit;
?>
<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
    $username = $_POST['username'];

    
    $stmt = $mysqli->prepare("select * from songs where artist = ?;");
    
    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

	$stmt->bind_param('s', $username);
    $stmt->execute();
    $query_result = $stmt->get_result();

    $output_json = array();
    $songs = array();

    while($row = $query_result->fetch_assoc()){
        $temp = array("artist" => $row["artist"], "soundcloud_src" => $row["soundcloud_src"], "likes" => $row["likes"], "listens" => $row["listens"]);
		$song_name = $row['name'];
    	$songs[$song_name] = $temp;
    }
    
    $output_json['songs'] = $songs;
    
    // now want to just add user info
    
	$query="select * from accounts where username = ?";
    $stmt = $mysqli->prepare($query);

    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

    $stmt->bind_param('s', $username);
    $stmt->execute();
    $query_result = $stmt->get_result();
    $row = $query_result->fetch_assoc();
	
	$user_info = array("username" => $row['username'], "bio" => $row['bio'], "image_src" => $row['image_src']);
	$output_json['user_info'] = $user_info;
    
    echo json_encode($output_json);
    $stmt->close();
    exit;
?>
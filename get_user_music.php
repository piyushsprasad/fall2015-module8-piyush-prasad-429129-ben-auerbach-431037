<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
    $username = $_POST['username'];

    
    $stmt = $mysqli->prepare("select * from songs where artist = ? order by id desc;");
    
    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

	$stmt->bind_param('s', $username);
    $stmt->execute();
    $query_result = $stmt->get_result();

    $output_json = array();

    while($row = $query_result->fetch_assoc()){
        $temp = array("artist" => $row["artist"], "soundcloud_src" => $row["soundcloud_src"], "likes" => $row["likes"], "listens" => $row["listens"]);
		$song_name = $row['name'];
    	$output_json[$song_name] = $temp;
    }
    
    echo json_encode($output_json);
    $stmt->close();
    exit;
?>
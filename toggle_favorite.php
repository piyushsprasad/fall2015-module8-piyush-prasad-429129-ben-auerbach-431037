<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
function is_favorite_now($song_name, $artist){
	global $mysqli;

    $query="select * from favorite_songs where name = ? and artist = ? and user_id = ?";
    $stmt = $mysqli->prepare($query);

    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

	// user_id is actually just username...which is the session variable
    $stmt->bind_param('sss', $song_name, $artist, $_SESSION['current_user']);
    $stmt->execute();
    $query_result = $stmt->get_result();
    $row = $query_result->fetch_assoc();

    if(!empty($row["name"])){
        error_log("ROW WAS NOT EMPTY!");
        return true;
    }

    $stmt->close();
    
    error_log("ROW WAS EMPTY!");

    return false;
}

function favorite($song_name, $artist){
    global $mysqli;

    $stmt = $mysqli->prepare("insert into favorite_songs (name, artist, user_id) values (?, ?, ?);");

    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }

    $stmt->bind_param('sss', $song_name, $artist, $_SESSION['current_user']);
    $stmt->execute();
    $stmt->close();
}

function unfavorite($song_name, $artist){
    global $mysqli;

    $stmt = $mysqli->prepare("delete from favorite_songs where name = ? and artist = ? and user_id = ?;");

    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }

    $stmt->bind_param('sss', $song_name, $artist, $_SESSION['current_user']);
    $stmt->execute();
    $stmt->close();
}

$song_name = $_POST['song_name'];
$artist = $_POST['artist'];

error_log("printing song_name");
error_log($song_name);
error_log('printing artist');
error_log($artist);

$is_favorite_now = is_favorite_now($song_name, $artist);

error_log("is_favorite_now value");
error_log($is_favorite_now);

if($is_favorite_now){
	unfavorite($song_name, $artist);
	error_log("favoriting");
}
else{
	favorite($song_name, $artist);
	error_log("UNFAVORITING");
}
?>

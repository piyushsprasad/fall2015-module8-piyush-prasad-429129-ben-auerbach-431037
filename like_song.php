<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
    $artist = $_POST['artist'];
    $song_name = $_POST['song_name'];
    
    
	$stmt = $mysqli->prepare("select likes from songs where name = ? and artist = ?;");

	if(!$stmt){
    	printf("Query Prep Failed: %s <br>", $mysqli->error);
    	exit;
	}

	$stmt->bind_param('ss', $song_name, $artist);
	$stmt->execute();
	$query_result = $stmt->get_result();

	$row = $query_result->fetch_assoc();
	$num_likes = $row['likes'];
	$stmt->close();
	
	// increasing the number of likes by 1
	$num_likes += 1;

	// now updating the number of likes to the new number
	$stmt = $mysqli->prepare("update songs set likes = ? where name = ? and artist = ?;");

	if(!$stmt){
    	printf("Query Prep Failed: %s\n", $mysqli->error);
    	exit;
	}

	$stmt->bind_param('iss', $num_likes, $song_name, $artist);
	$stmt->execute();
	$stmt->close();
?>
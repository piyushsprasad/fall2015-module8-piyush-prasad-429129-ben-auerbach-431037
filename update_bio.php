<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;

// if username exists in the database already, returns true
function update_bio($updated_bio, $username){
    global $mysqli;

    $query="update accounts set bio = ? where username = ?;";
    $stmt = $mysqli->prepare($query);

    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

    $stmt->bind_param('ss', $updated_bio, $username);
    $stmt->execute();
    $stmt->close();
}

$username = $_POST['username'];
$updated_bio = $_POST['updated_bio'];
update_bio($updated_bio, $username);
?>

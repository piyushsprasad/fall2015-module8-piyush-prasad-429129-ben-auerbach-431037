<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
    function upload_soundcloud($soundcloud_src, $song_name){
    	global $mysqli;

    	$stmt = $mysqli->prepare("insert into songs (artist, name, soundcloud_src, likes, listens) values (?, ?, ?, ?, ?);");

    	if(!$stmt){
        	printf("Query Prep Failed: %s\n", $mysqli->error);
        	exit;
    	}
    	
    	$val = 0;

    	$stmt->bind_param('sssii', $_SESSION['current_user'], $song_name, $soundcloud_src, $val, $val);
    	$stmt->execute();
    	$stmt->close();
    }
    
    $soundcloud_src = $_POST['soundcloud_src'];
    $song_name = $_POST['song_name'];
    upload_soundcloud($soundcloud_src, $song_name);
?>
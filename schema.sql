CREATE TABLE accounts (
	username varchar(30) not null,
	password varchar(250) not null,
	bio varchar(500)

	primary key (username)
	) engine = innodb;

CREATE TABLE pictures (
	username varchar(30) not null,
    image_type varchar(25) not null default '',
    image blob not null,
    image_size varchar(25) not null default '',
    image_ctgy varchar(25) not null default '',
    image_name varchar(50) not null default ''

	primary key (username)
	) engine = innodb;

CREATE TABLE songs (
	name varchar(30) not null,
	artist varchar(30) not null,
	soundcloud_src varchar(500),
	likes int not null,
	listens int not null

	primary key (username)
	) engine = innodb;

CREATE TABLE favorite_songs (
	name varchar(30) not null,
	artist varchar(30) not null,
	song_id int,
	user_id varchar(30) not null

	primary key (username)
	) engine = innodb;


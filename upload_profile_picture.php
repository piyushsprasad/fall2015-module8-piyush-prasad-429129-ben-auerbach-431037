<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
    function upload_profile_picture($image_src){
    	global $mysqli;

    	$query="update accounts set image_src = ? where username = ?;";
    	$stmt = $mysqli->prepare($query);

    	if(!$stmt){
       	 printf("Error with query: %s", $mysqli->error);
       	 exit;
    	}

    	$stmt->bind_param('ss', $image_src, $_SESSION['current_user']);
    	$stmt->execute();
    	$stmt->close();
	}
    
    $image_src = $_POST['image_src'];
    $username = $_POST['username'];
    if($username == $_SESSION['current_user']){
    	upload_profile_picture($image_src);
    }
?>
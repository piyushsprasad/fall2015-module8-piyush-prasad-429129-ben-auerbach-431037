<?php
	header("Content-Type: application/json");
	require 'music_database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();

    global $mysqli;
    
function username_exists($username){
    global $mysqli;

    $query="select username, password from accounts where username = ?";
    $stmt = $mysqli->prepare($query);

    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

    $stmt->bind_param('s', $username);
    $stmt->execute();
    $query_result = $stmt->get_result();
    $row = $query_result->fetch_assoc();

    if(!empty($row["username"])){
        return true;
    }

    $stmt->close();

    return false;

}
    
    $artist_search = $_POST['artist_search'];
    $result = username_exists($artist_search);
    
    if($result){   
    	echo json_encode(array("success" => true));
    }
    else{    
    	echo json_encode(array("success" => false));
    }
    exit;
?>
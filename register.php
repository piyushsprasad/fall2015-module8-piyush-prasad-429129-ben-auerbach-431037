<?php
header("Content-Type: application/json");
require 'music_database.php';

// if username exists in the database already, returns true
function username_exists($username){
    global $mysqli;

    $query="select username, password from accounts where username = ?";
    $stmt = $mysqli->prepare($query);

    if(!$stmt){
        printf("Error with query: %s", $mysqli->error);
        exit;
    }

    $stmt->bind_param('s', $username);
    $stmt->execute();
    $query_result = $stmt->get_result();
    $row = $query_result->fetch_assoc();

    if(!empty($row["username"])){
        return true;
    }

    $stmt->close();

    return false;

}

// function creates a new user and password in the database
function create_new_user($username, $password){
    global $mysqli;

    $stmt = $mysqli->prepare("insert into accounts (username, password, bio, image_src) values (?, ?, ?, ?);");

    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    
    $bio = "No bio yet!";
    $image_src = "";

    $stmt->bind_param('ssss', $username, $password, $bio, $image_src);
    $stmt->execute();
    $stmt->close();
}


if(empty($_POST['username'])){
    echo json_encode(array("success" => false, "message" => "No username entered."));
    exit;
}
else if(empty($_POST['password'])){
    echo json_encode(array("success" => false, "message" => "No password entered."));
    exit;
}
else{
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $encrypted_password = crypt($password);
    $current_day = $_POST['current_day'];
    $current_month = $_POST['current_month'];
    $current_year = $_POST['current_year'];

    $username_exists = username_exists($username);

    // if username already exists, asks them to choose another username
    if($username_exists){
        echo json_encode(array("success" => false, "message" => "Username already exists. Please try another."));
        exit;
    }
    else{
        create_new_user($username, $encrypted_password);
        //create_new_user($username, $password);
        ini_set("session.cookie_httponly", 1);
        session_start();
        $_SESSION['current_user'] = $username;
        // not sure what this means ****************
		$_SESSION['token'] = substr(md5(rand()), 0, 10);
		$_SESSION['current_day'] = $current_day;
		$_SESSION['current_month'] = $current_month;
		$_SESSION['current_year'] = $current_year;
        echo json_encode(array("success" => true));
        exit;
    }
}
?>
